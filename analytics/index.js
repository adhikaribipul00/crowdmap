var admin = require('firebase-admin');
var serviceAccount = require('./crowd-map-key.json');
var fs = require('fs');
var http = require('http');
var url = require('url');
var FastSet = require('collections/fast-set');
var Dict = require("collections/dict");
var List = require("collections/list");
var PubNub = require('pubnub');
const util = require('util');

//Firebase
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: 'https://crowd-map.firebaseio.com/'
});
var db = admin.database();

//Private data
//We push it as a set
var macs = new FastSet();

//Interval times of recordings
var timeInterval = new FastSet();

//Key = timestamp
//Value = array of macs at that time
var loc_a_data = new Dict();
var loc_b_data = new Dict();
var loc_c_data = new Dict();



var ref = db.ref("end_points/location_a/mac_set");
ref.on("value",function(snapshot) {
	console.log("Event happened at location b");
	Object.keys(snapshot.val()).forEach(function(key) {
		loc_b_data.add(snapshot.val()[key].length,key);
		timeInterval.add(key);
	});
	
});


/**
WEB Server part
**/
var pathHeader = "./public";

http.createServer(function (request,response) {
	var query = url.parse( request.url, true);
	var fname = pathHeader + query.pathname;
	//Ajax endpoint
	var aj= pathHeader + "/datapoints.json";
	if ( fname == aj ){
		console.log("AJAX call");
		response.writeHead(200, {'Content-Type' : 'application/json'});
		//To -do ajax implementation
		var xd = [];
		var yd = [];
		timeInterval.forEach(function(value){
			xd.push(value);
			yd.push(loc_b_data.get(value));
		});
		payload = { x:xd, y:yd }; 
		console.log("Payload = " + payload);
		response.write(JSON.stringify(payload));
		response.flush();
		return response.end();
		/*
		fs.readFile(pathHeader+'/datapoints.json', 'utf8', function(err,data){
			if ( err ){
				console.log("Reading file error" + err);
				return response.end();
			}
			response.write(data);
			response.flush();
			return response.end();
		});
		*/


	}
	else{
		fs.readFile(fname , function( err, data ){
			if ( err ){
				response.writeHead( 404, {'Content-Type' : 'text/html'} );
				console.log("Invalid request: " + fname);
				return response.end("404 Not found");
			}
			else if (fname.indexOf('css') > -1){
				response.writeHead(200, {'Content-Type':'text/css'});
				response.write(data);
				console.log('CSS file found:'+ fname);
				return response.end();
			}
			else if (fname.indexOf('js') > -1){
				response.writeHead(200, {'Content-Type':'text/javascript'});
				response.write(data);
				console.log('JS file found:'+ fname);
				return response.end();
			}
			else {
				response.writeHead(200, { 'Content-Type' : 'text/html' });
				response.write(data);
				console.log('Normal File found:'+ fname);
				return response.end();
			}
		});
	}
}).listen(9999);

