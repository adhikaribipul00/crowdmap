var set = new Set();
var keys;
var x = [];
var y = [];

var ctx = document.getElementById("chart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
    	//Here the labels should be time
        labels: x,
        datasets: [{
            label: 'Number of Users',
	    //Data should be the numer of devices
            data: y,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

$(document).ready(function(){
	setInterval(function(){
		console.log("Calling ajax");
		fetch('datapoints.json')
		.then(function(response){
				return response.text().then(function(text){
					set.add(text);
				});
		});
		var data = [];
		set.forEach(function(value){
			var parsed = JSON.parse(value);
			data =  Object.values(parsed);
		});
//		resetCanvas();
		myChart.data.datasets[0].data = data[1];
		myChart.data.labels = data[0];
		console.log(data[0]);
		console.log(data[1]);
		myChart.update();
		},2500);
		
});

	

